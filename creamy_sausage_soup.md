# Creamy Sausage Soup

## Ingredients

+ 2 lbs. sausage
+ 1/2 cup copped carrots
+ 2 cloves diced garlic
+ 2 stalks chopped celery
+ 1/2 cup broccoli florets
+ 1-1/2 cup spinach
+ 4 cups chicken stock
+ 1 cup heavy cream
+ 4 oz. cream cheese

## Instructions

1. Set instant pot to saute and cook the sausage and garlic until sausage is browned
2. Add veggies and chicken stock to instant pot and set to high pressure for 5 minutes
3. When cooking is complete, quick release the pressure and stir in heavy cream and cheese until fully incorporated