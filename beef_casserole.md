# Ground Beef Casserole

## Ingredients

+ 1 lb. ground beef
+ 3 oz. cream cheese
+ 1/2 cup heavy cream
+ 1/2 cup beef broth
+ 1 tsp. garlic powder
+ 1 tsp. salt
+ 1/4 tsp. freshly cracked black pepper
+ 2 cans green beans
+ 3/4 cup shredded mozerella
+ 3/4 cup shredded cheddar
+ 3 slices of bacon, cooked and crumbled

## Instructions

1. In a cast-iron skillet, brown the beef and drain excess fat
2. Add cream cheese and mix with beef until melted
3. Add beef broth, heavy cream, garlic powder, salt and pepper
4. Bring mixture to a boil on medium heat until sauce begins to thicken
5. Reduce heat and simmer until sauce is thickend
6. Add green beans to skillet then top with cheese
7. Bake at 350ºF for 20 minutes
8. Top with crumbled bacon