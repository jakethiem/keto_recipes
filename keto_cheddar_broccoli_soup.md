# Keto Cheddar Broccoli Soup

## Ingredients

+ broccoli florets from 1 head of broccoli (~1/2 pounds)
+ 1 cup diced onions
+ 2 cups shredded sharp cheddar cheese
+ 4 cups chicken stock
+ 1 tsp sea salt (to taste)
+ 1/4 tsp black pepper (to taste)
+ 1 cup heavy cream
+ 4 oz. cream cheese
+ diced crispy bacon

## Instructions

1. Add onions, broccoli, cheddar, chicken stock, salt and pepper to the instant pot and close the lid
2. Set to manual at high pressure for 7 minutes
3. When cooking is complete, quick release the pressure
4. Microwave the cream cheese for \~30 seconds to soften
5. Transfer 1 cup of soup to a blender with the 1 cup of heavy cream and cream cheese
6. Blend until smooth then transfer back into soup
7. Set instant pot to saute and simmer for 4-5 minutes until soup has thickened
8. Serve with shredded cheese and diced bacon